//
//  LogController.swift
//  ApiProject
//
//  Created by Alexandre Bétourné on 29/03/2019.
//  Copyright © 2019 betournator. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth


class LogController: UIViewController {

    @IBOutlet weak var emailField: UITextField! {
        didSet {
            emailField.tintColor = UIColor.lightGray
            emailField.setIcon(UIImage(named: "mail")!)
        }
    }
    
    @IBOutlet weak var passwordField: UITextField! {
        didSet {
            passwordField.tintColor = UIColor.lightGray
            passwordField.setIcon(UIImage(named: "pass")!)
        }
    }
    
    
    
    @IBAction func signinButton(_ sender: Any) {
        Auth.auth().signIn(withEmail: emailField.text!, password: passwordField.text!) { (user, error) in
            if(error == nil )
            {
                if let user = user {
                    _ = user.user.displayName
                    let user_email = user.user.email
                }
                
                self.performSegue(withIdentifier: "loginToList", sender: nil)
            }
            else{
                if let errorCode = AuthErrorCode(rawValue: error!._code) {
                    switch errorCode {
                    case.wrongPassword:
                        self.showAlert(message: "Wrong Password, try again!")
                        break
                    case.userNotFound:
                        Auth.auth().createUser(withEmail: self.emailField.text!, password: self.passwordField.text!,
                                               completion: { (user, error) in
                                                if error == nil {
                                                    if let user = user {
                                                        _=user.user.displayName
                                                        let user_email = user.user.email
                                                    }else {
                                                        if let errorCode = AuthErrorCode(rawValue: error!._code) {
                                                            switch errorCode {
                                                            case .invalidEmail:
                                                                self.showAlert(message: "You entered an invalid email!")
                                                            case .userNotFound:
                                                                self.showAlert(message: "User not found")
                                                            default:
                                                                self.showAlert(message: "Unexpected error \(errorCode.rawValue) please try again!")
                                                            }
                                                        }
                                                    }
                                                    self.dismiss(animated: true, completion: nil)
                                                }
                        })
                    default:
                        self.showAlert(message: "Unexpected error \(errorCode.rawValue) please try again!")
                    }
                }
            }
        }
    }
    
    
    @IBAction func signupButton(_ sender: Any) {
        if ((emailField.text == "") || (passwordField.text == "")) {
            self.showAlert(message: "All fields are mandatory!")
            return
        } else {
            Auth.auth().createUser(withEmail: emailField.text!, password: passwordField.text!) {
                (authResult, error) in
                if ((error == nil)) {
                    self.showAlert(message: "Signup Successfully!")
                } else {
                    self.showAlert(message: "\(error)")
                }
            }
        }
    }
    
    func showAlert(message:String)
    {
        let alert = UIAlertController(title: message, message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailField.underlined()
        passwordField.underlined()
        
        btn1.layer.cornerRadius = 18
        btn2.layer.cornerRadius = 18
    }
    


}

extension UITextField {
    func setIcon(_ image: UIImage) {
        let iconView = UIImageView(frame:
            CGRect(x: 0, y: 5, width: 20, height: 20))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame:
            CGRect(x: 20, y: 0, width: 30, height: 30))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
    }
    
    func underlined(){
        let border = CALayer()
        let width = CGFloat(0.5)
        border.borderColor = UIColor(red: 178.0 / 255.0, green: 178.0 / 255.0, blue: 178.0 / 255.0, alpha: 1.0).cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }

}
