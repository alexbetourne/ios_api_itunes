//
//  FavViewController.swift
//  ApiProject
//
//  Created by Alexandre Bétourné on 03/04/2019.
//  Copyright © 2019 betournator. All rights reserved.
//

import UIKit

class FavViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    var favs:[Albums] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath ) as! TableViewCell
        let fav = favs[indexPath.row]
        
        cell.title.text! = fav.title
        cell.artist_label.text! = fav.artist
        let urlImg = URL(string: fav.album_img)
        let dataImg = try? Data(contentsOf: urlImg!)
        cell.album_img.image = UIImage(data : dataImg!)
        cell.album.text! = fav.album
        
        return cell
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "My Favorites"
        navigationController?.navigationBar.prefersLargeTitles = true
        
    }

    
    
    override func viewWillAppear(_ animated: Bool) {
        getFavsData()
        tableView.reloadData()
    }
    
    func pushData(data: Albums){
        favs.append(data)
    }
    
    func getFavsData(){
        print(favs)
    }

}
