//
//  PlayerViewController.swift
//  ApiProject
//
//  Created by Alexandre Bétourné on 11/04/2019.
//  Copyright © 2019 betournator. All rights reserved.
//

import UIKit
import AVFoundation

class PlayerViewController: UIViewController {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var popup: UIView!
    @IBOutlet weak var artistLabel: UILabel!
    
    var songurl = ""
    var imgurl = ""
    var titleText = ""
    var artistText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        
        popup.layer.shadowColor = UIColor.black.cgColor
        popup.layer.shadowOffset = CGSize(width: 10, height: 10)
        popup.layer.shadowOpacity = 0.6
        popup.layer.shadowRadius = 10.0
        popup.layer.cornerRadius = 10
        
        self.showAnimate()
        
        playSong(urlString: songurl)
        
        let urlImg = URL(string: imgurl)
        let dataImg = try? Data(contentsOf: urlImg!)
        
        img.image = UIImage(data : dataImg!)
        img.layer.cornerRadius = 10
        
        
        textLabel.text = titleText
        artistLabel.text = artistText
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func playSong(urlString : String){
        let fileUrl = NSURL(string: urlString)
        playUsingAVPlayer(url: fileUrl! as URL, isPlaying: false)
    }
    
    func playUsingAVPlayer(url: URL, isPlaying: Bool) {
        player = AVPlayer(url: url)
        
        if isPlaying {
            player?.pause()
        } else {
            player?.play()
        }
        
    }
    
    
    
    
    
    
    @IBAction func closeButton(_ sender: Any) {
        self.removeAnimate()
        player?.pause()
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
}
