//
//  ViewController.swift
//  ApiProject
//
//  Created by Alexandre Bétourné on 15/03/2019.
//  Copyright © 2019 betournator. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Foundation
import AVFoundation

var player: AVPlayer?


class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {

    var albums:[Albums] = []
    var favorites:[Albums] = []
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputText: UITextField!
    
    var limit = 10
    var actualArtist = "13 Block"
    var favisopen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Music Search"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Favorites", style: .plain, target: self, action: #selector(runFavs))

        inputText.returnKeyType = UIReturnKeyType.search
        inputText.delegate = self
        
        initData(artist: actualArtist, maxData: limit)
        
        
    }
    
    
    
    
    
    @objc func runFavs(){
        if favisopen == true {
            favisopen = false
            tableView.reloadData()
        } else {
            favisopen = true
            tableView.reloadData()
        }
    }
    
    
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if favisopen == true {
            return favorites.count
        } else {
            return albums.count
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if favisopen == false {
            let lastItem = albums.count-1
            if indexPath.row == lastItem{
                if limit <= 49 {
                    loadMoreData()
                }
            }
        }
    }
    
    func loadMoreData(){
        limit = limit + 10
        initData(artist: actualArtist, maxData: limit)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath ) as! TableViewCell
        
        var album = albums[0]
        
        if favisopen == true {
            album = favorites[indexPath.row]
        } else {
            album = albums[indexPath.row]
        }
        
        
        cell.favButton.tag = indexPath.row
        cell.favButton.addTarget(self, action: #selector(favButtonClicked(sender:)), for: .touchUpInside)

        cell.title.text! = album.title
        cell.artist_label.text! = album.artist
        let urlImg = URL(string: album.album_img)
        let dataImg = try? Data(contentsOf: urlImg!)
        cell.album_img.image = UIImage(data : dataImg!)
        cell.album.text! = album.album
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "playerPopUp") as! PlayerViewController
        popOverVC.songurl = albums[indexPath.row].song
        popOverVC.imgurl =  albums[indexPath.row].album_img
        popOverVC.titleText =  albums[indexPath.row].title
        popOverVC.artistText = albums[indexPath.row].artist
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    
    
    
    
    
    @objc private func favButtonClicked(sender : UIButton){
        let array:Array<Any>
        if !favisopen {
            var alert:UIAlertController
            if albums[sender.tag].fav == true {
//                array.remove(at: sender.tag)
                favorites.remove(at: sender.tag)
                alert = UIAlertController(title: "Retiré des favoris.", message: "Le son a été retiré des favoris avec succès.", preferredStyle: UIAlertController.Style.alert)
                albums[sender.tag].fav = false
                sender.isSelected = false
            } else {
                favorites.append(albums[sender.tag])
                alert = UIAlertController(title: "Ajouté aux favoris.", message: "Le son a été ajouté aux favoris avec succès.", preferredStyle: UIAlertController.Style.alert)
                albums[sender.tag].fav = true
                sender.isSelected = true
            }
            
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            favorites.remove(at: sender.tag)
            tableView.reloadData()
            let alert = UIAlertController(title: "Retiré des favoris.", message: "Le son a été retiré des favoris avec succès.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
//        let favoritesData = NSKeyedArchiver.archivedData(withRootObject: favorites)
//        UserDefaults.standard.set(favorites, forKey: "favs")
        
        
//        UserDefaults.standard.set(array, forKey: "SavedArray")
//        let favsData = UserDefaults.standard.object(forKey: "SavedArray")
//        print(favsData!)
    }
    
    
    
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 15
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        return newString.length <= maxLength
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        actualArtist = inputText.text!
        tableView.setContentOffset(.zero, animated: true)
        limit = 10
        initData(artist: actualArtist, maxData: limit)
        textField.resignFirstResponder()
        return true
    }
    
    
    
    
    
    func initData(artist:String, maxData: Int){
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        self.view.addSubview(activityIndicator)
        activityIndicator.center = CGPoint(x: self.view.frame.size.width*0.5, y: self.view.frame.size.height*0.5)
        activityIndicator.startAnimating()
        
        albums.removeAll()
        tableView.isHidden = true
        
        if artist == "" {
            let alert = UIAlertController(title: "Champs vide", message: "Entrez une valeur svp", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        } else {
            let newArtist = artist.replacingOccurrences(of: " ", with: "-", options: .literal, range: nil)
            
            
            Alamofire.request("https://itunes.apple.com/search?term=\(newArtist)", method: .get, encoding: URLEncoding.default).responseJSON { response in
                switch response.result {
                case .success:
                    
                    if let json = response.result.value {
                        let j = JSON(json)
                        
                        if j["results"].isEmpty {
                            let alert = UIAlertController(title: "Pas de résulats", message: "Désolé nous n'avons pas trouvé de résultats pour votre recherche", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        } else {
                            for i in 0..<maxData
                            {
                                let artistName = j["results"][i]["artistName"].stringValue
                                let trackName = j["results"][i]["trackName"].stringValue
                                let img = j["results"][i]["artworkUrl100"].stringValue
                                let albumName = j["results"][i]["collectionName"].stringValue
                                let song = j["results"][i]["previewUrl"].stringValue
                            
                                
                                let date = j["results"][i]["releaseDate"].stringValue
                                
                                guard let year = date.range(of: "-")?.lowerBound else {
                                    return
                                }
                                
                                let substring = date[..<year]
                                let yearString = String(substring)
                                
                                
                                let scar = Albums()
                                
                                scar.artist = "\(artistName)"
                                scar.title = "\(trackName)"
                                scar.album_img = img
                                scar.album = "\(albumName) - \(yearString)"
                                scar.song = "\(song)"
                                
                                self.albums.append(scar)
                            }
                        }
                        
                        
                        activityIndicator.stopAnimating()
                        activityIndicator.removeFromSuperview()
                    }
                    
                    DispatchQueue.main.async {
                        activityIndicator.stopAnimating()
                        activityIndicator.removeFromSuperview()
                        self.tableView.reloadData()
                        self.tableView.isHidden = false
                    }
                case .failure(let error):
                    print("Error: \(error)")
                }
            }
        }
    }
}

