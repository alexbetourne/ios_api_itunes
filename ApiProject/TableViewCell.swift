//
//  TableViewCell.swift
//  ApiProject
//
//  Created by Alexandre Bétourné on 15/03/2019.
//  Copyright © 2019 betournator. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var album_img: UIImageView!
    @IBOutlet weak var artist_label: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var album: UILabel!
    @IBOutlet weak var favButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
